//package repository
//
//import com.todo.ServerTest
//import com.todo.repository.RecordInvalidException
//import com.todo.repository.TodoRepository
//import com.todo.table.CreateTodo
//import com.todo.table.EditTodo
//import com.todo.table.TodosConstant.Companion.ID_START
//import kotlinx.coroutines.runBlocking
//import org.assertj.core.api.Assertions
//import org.assertj.core.api.Assertions.assertThat
//import org.junit.jupiter.api.Nested
//import org.junit.jupiter.api.Test
//
//class TodoRepositoryTest : ServerTest() {
//
//    private val todoRepository = TodoRepository()
//
////    @BeforeEach
////    fun `各テストはTodoレコード0件を前提に作成しているためレコード数を確認する`() {
////        assertThat(todoRepository.countTodo()).isEqualTo(0)
////    }
//
//    @Test
//    fun getAllTodo() = runBlocking {
//        //登録データ
//        val todo1 = CreateTodo("テスト1", "テスト詳細1", "2022-01-01")
//        val todo2 = CreateTodo("テスト2", "テスト詳細2", "2022-02-02")
//        val todo3 = CreateTodo("テスト3", "テスト詳細3", "2022-03-03")
//        todoRepository.create(todo1)
//        todoRepository.create(todo2)
//        todoRepository.create(todo3)
//
//        //テーブルを取得
//        val retrieved = todoRepository.findAll()
//
//        //登録データの件数を確認
//        assertThat(todoRepository.countTodo()).isEqualTo(3)
//        assertThat(retrieved).extracting("title").containsExactlyInAnyOrder(todo1.title, todo2.title, todo3.title)
//        assertThat(retrieved).extracting("detail").containsExactlyInAnyOrder(todo1.detail, todo2.detail, todo3.detail)
//
//        Unit
//    }
//
//    @Test
//    //データが作成されるか確認テスト
//    fun addTodo() = runBlocking {
//        //登録データ
//        val newTodo = CreateTodo("テスト新規登録", "テスト新規登録1", "2022-01-01")
//
//        //データ作成
//        todoRepository.create(newTodo)
//
//        //登録データの内容確認
//        val retrieved = todoRepository.getTodo(ID_START)
//        assertThat(todoRepository.countTodo()).isEqualTo(1)
//        assertThat(retrieved?.id).isEqualTo(ID_START)
//        assertThat(retrieved?.title).isEqualTo(newTodo.title)
//        assertThat(retrieved?.detail).isEqualTo(newTodo.detail)
//
//        Unit
//    }
//
//    @Test
//    //登録したデータが更新されるか確認
//    fun updateTodo() = runBlocking {
//        //更新前データ
//        val oldTodo = CreateTodo("テスト更新前", "テスト更新前", "2022-01-01")
//        todoRepository.create(oldTodo)
//
//        //更新後データ
//        val newTodo = EditTodo("updatedTitle", "updatedDetail", "2020-02-02")
//        todoRepository.edit(ID_START, newTodo)
//
//        //更新内容を確認
//        val retrieved = todoRepository.getTodo(ID_START)
//        assertThat(todoRepository.countTodo()).isEqualTo(1)
//        assertThat(retrieved?.id).isEqualTo(ID_START)
//        assertThat(retrieved?.title).isEqualTo(newTodo.title)
//        assertThat(retrieved?.detail).isEqualTo(newTodo.detail)
//
//        Unit
//    }
//
//    @Test
//    //データが削除されるか確認
//    fun deleteTodo() = runBlocking {
//        //登録データ
//        val newTodo = CreateTodo("テスト削除確認", "テスト削除確認", "1900-01-01")
//        todoRepository.create(newTodo)
//
//        //データ削除
//        todoRepository.delete(ID_START)
//
//        //データが削除されているか確認
//        assertThat(todoRepository.countTodo()).isEqualTo(0)
//
//        Unit
//    }
//
//    @Nested
//    inner class ErrorCases {
//
//        @Test
//        fun `Todo登録時のTitleが100文字以上の場合、例外が発生する`() = runBlocking {
//            //登録データ
//            val invalidTitle =
//                "titleeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"
//            Assertions.assertThat(invalidTitle.length).isEqualTo(101)
//            val newTodo = CreateTodo(invalidTitle, "detail", "2020-01-01")
//
//            //登録処理
//            val thrown: Throwable = Assertions.catchThrowable {
//                todoRepository.create(newTodo)
//            }
//
//            //結果
//            Assertions.assertThat(thrown)
//                .isInstanceOf(RecordInvalidException::class.java)
//
//            Unit
//        }
//
//        @Test
//        fun `Todo登録時のDetailが1000文字以上の場合、例外が発生する`() = runBlocking {
//            //登録データ
//            val invalidDetail =
//                "detaillllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll"
//            Assertions.assertThat(invalidDetail.length).isEqualTo(1001)
//            val newTodo = CreateTodo("title", invalidDetail, "2020-01-01")
//
//            //登録処理
//            val thrown: Throwable = Assertions.catchThrowable {
//                todoRepository.create(newTodo)
//            }
//
//            //結果
//            Assertions.assertThat(thrown)
//                .isInstanceOf(RecordInvalidException::class.java)
//
//            Unit
//        }
//
//        @Test
//        fun `Todo更新時に対象のTodoが存在しない場合、例外が発生する`() = runBlocking {
//            //更新データ
//            val newTodo = EditTodo("updatedTitle", "updatedDetail", "2020-02-02")
//            //更新処理
//            val thrown: Throwable = Assertions.catchThrowable {
//                todoRepository.edit(ID_START, newTodo)
//            }
//
//            //結果
//            Assertions.assertThat(thrown)
//                .isInstanceOf(RecordInvalidException::class.java)
//
//            Unit
//        }
//
//
//        @Test
//        fun `Todo削除時に対象のTodoが存在しない場合、例外が発生する`() = runBlocking {
//            //削除処理
//            val thrown: Throwable = Assertions.catchThrowable {
//                todoRepository.delete(ID_START)
//            }
//            //結果
//            Assertions.assertThat(thrown)
//                .isInstanceOf(RecordInvalidException::class.java)
//
//            Unit
//        }
//    }
//}