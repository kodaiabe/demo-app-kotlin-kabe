package route

import com.todo.ServerTest
import com.todo.module
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.server.testing.*
import org.junit.Assert.assertEquals
import org.junit.jupiter.api.Test

class TodoRouteTest: ServerTest() {

    //成功（GET）
    @Test
    fun testFindAllTodos(){
        withTestApplication(Application::module) {
            handleRequest(HttpMethod.Get, "/todos").apply {
                assertEquals(HttpStatusCode.OK, response.status())
            }
        }
    }

    //成功（POST)
    @Test
    fun testCreateSuccessRequest(){
        //テーブルに登録出来る正常な値を記入
        val str = "{\"title\":\"タイトル2\",\"detail\":\"詳細2\",\"date\":\"2022-1-20\"}"
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Post,"/todos"){
                addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                setBody(str)
            }.run {
                assertEquals(HttpStatusCode.OK,response.status())
            }
        }
    }

    //失敗（POST)
    @Test
    fun testCreateErrorRequest(){
        //テーブルに登録されない不正な値を設定
        val str = "{\"title\":\"title\",\"detail\":\"detail\",\"date\":\"アイウエオ\"}"
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Post,"/todos"){
                addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                setBody(str)
            }.run {
                assertEquals(HttpStatusCode.InternalServerError,response.status())
            }
        }
    }

    //成功（PUT）
    @Test
    fun testEditSuccessRequest(){
        //データは更新されるけどエラーになる
        val str = "{\"title\":\"変更2\",\"detail\":\"変更2\",\"date\":\"2022-1-18\"}"
        withTestApplication(Application::module){
            //todos/(ここにDBに存在するidを記入)
            handleRequest(HttpMethod.Put,"/todos/5"){
                addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                setBody(str)
            }.run {
                assertEquals(HttpStatusCode.OK,response.status())
            }
        }
    }

    //失敗（PUT)
    @Test
    fun testPutErrorRequest(){
        //テーブルに登録されない不正な値を設定
        val str = "{\"title\":\"title\",\"detail\":\"detail\",\"date\":\"アイウエオ\"}"
        withTestApplication(Application::module){
            //todos/(ここにDBに存在しないidを記入)
            handleRequest(HttpMethod.Put,"/todos/16666"){
                addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                setBody(str)
            }.run {
                assertEquals(HttpStatusCode.InternalServerError,response.status())
            }
        }
    }


    //成功（DELETE）
    @Test
    fun testDeleteSuccessRequest(){
        withTestApplication(Application::module){
            //todos/(ここにDBに存在するidを記入)
            handleRequest(HttpMethod.Delete,"/todos/4"){
                addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            }.run {
                assertEquals(HttpStatusCode.OK,response.status())
            }
        }
    }

    //失敗(DELETE）
    @Test
    fun testDeleteErrorRequest(){
        withTestApplication(Application::module){
            //todos/(ここに検索できない文字列を記入)
            handleRequest(HttpMethod.Delete,"/todos/a1a2a"){
                addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            }.run {
                assertEquals(HttpStatusCode.InternalServerError,response.status())
            }
        }
    }
}