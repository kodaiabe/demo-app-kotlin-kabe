package com.todo

import com.todo.table.DatabaseInfo
import com.todo.repository.SystemException
import com.todo.route.todos
import io.ktor.application.*
import io.ktor.routing.*
import com.fasterxml.jackson.databind.*
import com.todo.repository.TodoRepository
import io.ktor.jackson.*
import io.ktor.features.*
import io.ktor.response.*


fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {

    //DBに接続
    DatabaseInfo.dbInfo()

    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
        }
    }

    val todoRepository = TodoRepository()

    install(Routing){
        todos(todoRepository)
    }

    install(StatusPages) {
        exception<SystemException> { cause ->
            call.response.status(cause.status)
            call.respond(cause.response())
        }
    }
}
