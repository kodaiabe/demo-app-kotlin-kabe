package com.todo.table

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.SchemaUtils.create

object DatabaseInfo {
    fun dbInfo(){
        Database.connect(
            url = "jdbc:mysql://127.0.0.1/todo",
            driver = "com.mysql.cj.jdbc.Driver",
            user = "root",
            password =  "password"
        )

        //テーブルを作ってくれる
        transaction {
            create(Todos)
        }
    }
}