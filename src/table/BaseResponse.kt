package com.todo.table

interface BaseResponse {
        val errorCode:Int
        val errorMessage:String
}
