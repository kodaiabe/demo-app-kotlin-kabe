package com.todo.table

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.dao.LongIdTable
import org.jetbrains.exposed.sql.CurrentDateTime
//import org.jetbrains.exposed.sql.CurrentDateTime
import org.jetbrains.exposed.sql.Table

object Todos : LongIdTable() {
    //val id = long("id").autoIncrement().check { it greaterEq TodosConstant.ID_START }
    val title = varchar("title", TodosConstant.TITLE_MAX_LENGTH)
    val detail = varchar("detail", TodosConstant.DETAIL_MAX_LENGTH).nullable()
    val date = date("date").nullable()
    private val createdAt = datetime("created_at").defaultExpression(CurrentDateTime())
    private val updatedAt = datetime("updated_at").defaultExpression(CurrentDateTime())
}

class TodosConstant {
    companion object {
        const val ID_START = 1L
        const val TITLE_MAX_LENGTH = 100
        const val DETAIL_MAX_LENGTH = 1000
    }
}

data class Todo(
    val id: Long,
    val title: String,
    val detail: String?,
    val date: String?
)

data class CreateTodo(
    val title: String,
    val detail: String?,
    val date: String?
)

data class EditTodo(
    val title: String,
    val detail: String?,
    val date: String?
)

data class TodoResponse(
    @JsonProperty("error_code")
    override val errorCode: Int = 0,
    @JsonProperty("error_message")
    override val errorMessage: String = ""
) : BaseResponse