package com.todo.route

import com.fasterxml.jackson.core.JsonParseException
import com.todo.table.CreateTodo
import com.todo.table.EditTodo
import com.todo.table.TodoResponse
import com.todo.repository.*
import com.todo.repository.ErrorCode.Companion.DELETE_ERROR_CODE
import com.todo.repository.ErrorCode.Companion.GET_ERROR_CODE
import com.todo.repository.ErrorCode.Companion.POST_ERROR_CODE
import com.todo.repository.ErrorCode.Companion.PUT_ERROR_CODE
import com.todo.repository.ErrorMessage.Companion.DELETE_ERROR_MESSAGE
import com.todo.repository.ErrorMessage.Companion.GET_ERROR_MESSAGE
import com.todo.repository.ErrorMessage.Companion.POST_ERROR_MESSAGE
import com.todo.repository.ErrorMessage.Companion.PUT_ERROR_MESSAGE
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import java.lang.Exception

fun Route.todos(todoRepository : TodoRepository) {

    route("/todos") {
        get {
            try {
//                call.respond(todoRepository.findAll())
//                call.respond(HttpStatusCode.OK, TodoResponse())
                val result = todoRepository.findAll()
                call.respond(HttpStatusCode.OK, result)
            }catch (e: Exception){
                //例外処理　水野さんのリポジトリを参考
                when(e){
                    is RecordInvalidException -> {
                        throw InternalServerErrorException(GET_ERROR_MESSAGE,GET_ERROR_CODE)
                    }
                    else -> throw UnknownException()
                }
            }
        }

        post{
            try {
                val createTodo = call.receive<CreateTodo>()
                todoRepository.create(createTodo)
                call.respond(HttpStatusCode.OK, TodoResponse())
            }catch (e:Exception){
                when (e) {
                    is JsonParseException -> {
                        throw BadRequestException()
                    }
                    is RecordInvalidException -> {
                        throw InternalServerErrorException(POST_ERROR_MESSAGE, POST_ERROR_CODE)
                    }
                    else -> throw UnknownException()
                }
            }
        }

        put("/{id}") {
            try {
                val id = call.parameters["id"]!!.toLong()
                val editTodo = call.receive<EditTodo>()
                todoRepository.edit(id, editTodo)
                call.respond(HttpStatusCode.OK, TodoResponse())
            }catch (e:Exception){
                when (e) {
                    is NullPointerException, is JsonParseException -> {
                        throw BadRequestException()
                    }
                    is RecordInvalidException -> {
                        throw InternalServerErrorException(PUT_ERROR_MESSAGE, PUT_ERROR_CODE)
                    }
                    else -> throw UnknownException()
                }
            }
        }

        delete("/{id}"){
            try {
                val id = call.parameters["id"]?.toLong() ?:return@delete
                todoRepository.delete(id)
                call.respond(HttpStatusCode.OK, TodoResponse())
            }catch (e:Exception){
                when (e) {
                    is NullPointerException -> {
                        throw BadRequestException()
                    }
                    is RecordInvalidException -> {
                        throw InternalServerErrorException(DELETE_ERROR_MESSAGE, DELETE_ERROR_CODE)
                    }
                    else -> throw UnknownException()
                }
            }
        }
    }
}

