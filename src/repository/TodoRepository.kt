package com.todo.repository


import com.todo.table.CreateTodo
import com.todo.table.EditTodo
import com.todo.table.Todo
import com.todo.table.Todos
import org.jetbrains.exposed.exceptions.ExposedSQLException
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.lang.Exception

class TodoRepository {
    //テーブル全体のデータを見る
    fun findAll():List<Todo> {
        try {
            return transaction {
                Todos.selectAll().sortedBy { it[Todos.date] }.map {
                    Todo(
                        id = it[Todos.id].value,
                        title = it[Todos.title].toString(),
                        detail = it[Todos.detail].toString(),
                        date = it[Todos.date].toString()
                    )
                }
            }
        } catch (e: Exception) {
            //例外処理
            when (e) {
                is ExposedSQLException -> throw RecordInvalidException()
                else -> throw e
            }
        }
    }

    //データを新規登録
    fun create(todo: CreateTodo) {
        try {
            val formatter = DateTimeFormat.forPattern("yyyy-MM-dd")
            transaction {
                Todos.insert {
                    it[title] = todo.title
                    it[detail] = todo.detail
                    it[date] = DateTime.parse(todo.date, formatter)
                }
            }
        } catch (e: Exception) {
            when (e) {
                is IllegalArgumentException, is ExposedSQLException -> throw RecordInvalidException()
                else -> throw e
            }
        }
    }


    //データを更新
    fun edit(id: Long, todo: EditTodo) {
        try {
            val formatter = DateTimeFormat.forPattern("yyyy-MM-dd")
            transaction {
                Todos.update(where = { Todos.id eq id }) {
                    it[title] = todo.title
                    it[detail] = todo.detail
                    it[date] = DateTime.parse(todo.date, formatter)
                }
            }
        } catch (e: Exception) {
            when (e) {
                is IllegalArgumentException, is RecordInvalidException, is ExposedSQLException -> {
                    throw RecordInvalidException()
                }
                else -> throw e
            }
        }
    }

    //データを削除
    fun delete(id: Long) {
        try {
            transaction {
                val isFailed = Todos.deleteWhere { Todos.id eq id } == 0
                if(isFailed) throw RecordInvalidException()
            }
        } catch (e: Exception) {
            when (e) {
                is RecordInvalidException, is ExposedSQLException -> throw RecordInvalidException()
                else -> throw e
            }
        }
    }

//    fun getTodo(id: Long) = transaction {
//        Todos.select {
//            (Todos.id eq id)
//        }.mapNotNull {
//            makeTodo(it)
//        }.singleOrNull()
//    }
//
//    //全件カウント
//    fun countTodo() = transaction {
//        Todos.selectAll().count()
//    }
//
//    private fun makeTodo(row: ResultRow) =
//        Todo(
//            id = row[Todos.id].value,
//            title = row[Todos.title],
//            detail = row[Todos.detail],
//            date = row[Todos.date].toString()
//        )
}

